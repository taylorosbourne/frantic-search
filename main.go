package main

import (
	"gitlab.com/taylorosbourne/frantic_search/cmd"
)

func main() {
	cmd.Execute()
}
