package cmd

import (
	"fmt"
	"os"

	"github.com/mgutz/ansi"
	"github.com/spf13/cobra"
	constants "gitlab.com/taylorosbourne/frantic_search/utils"
)

var rootCmd = &cobra.Command{
	Use:   "frs",
	Short: "the root command",
	Long:  constants.ABOUT_FRS,
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		// No args? Give guidance
		if len(args) == 0 {
			fmt.Println(cmd.Long)
			fmt.Println("\ntry: " + ansi.Color("frs search cards ", "green") + ansi.Color("--name ", "cyan") + ansi.Color("'goblin electromancer'", "yellow"))
		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
