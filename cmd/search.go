package cmd

import (
	"fmt"
	"time"

	"github.com/mgutz/ansi"
	"github.com/spf13/cobra"
	constants "gitlab.com/taylorosbourne/frantic_search/utils"
)

var Name string
var Color string
var Block string
var Legalities bool
var Limit int
var YearMax int
var YearMin int

func init() {
	currentTime := time.Now()

	cardsCmd.Flags().StringVarP(&Name, "name", "n", "", "search for cards by their name")
	cardsCmd.Flags().StringVarP(&Color, "color", "c", "", "search for cards by their color")
	cardsCmd.Flags().IntVarP(&Limit, "max", "m", 10, "limit the number of results")
	cardsCmd.Flags().BoolVarP(&Legalities, "legalities", "l", false, "search for a card despite its format legalities")
	cardsCmd.Flags().StringVarP(&Block, "block", "b", "", "search for cards by the block they appear in")

	setsCmd.Flags().StringVarP(&Name, "name", "n", "", "search for sets by their name")
	setsCmd.Flags().IntVarP(&YearMax, "year-max", "x", currentTime.Year(), "search for sets released during or before this year")
	setsCmd.Flags().IntVarP(&YearMin, "year-min", "m", 1993, "search for sets released during or after this year")
	setsCmd.Flags().StringVarP(&Block, "block", "b", "", "search for sets by the block they appear in")

	formatsCmd.Flags().StringVarP(&Name, "name", "n", "", "search for formats by their name")

	searchCmd.AddCommand(cardsCmd)
	searchCmd.AddCommand(setsCmd)
	searchCmd.AddCommand(formatsCmd)
	rootCmd.AddCommand(searchCmd)
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

var searchCmd = &cobra.Command{
	Use:       "search",
	Short:     "Initiates a Frantic Search",
	Long:      "\nThe search command tells frs what you want to search\nThis command requires one argument",
	ValidArgs: []string{"cards", "sets", "formats"},
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("Requires one argument")
		}
		if contains(cmd.ValidArgs, args[0]) {
			return nil
		}
		return fmt.Errorf("Invalid arg %s", ansi.Color(args[0], "red"))
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("\nSearch stuff")
	},
}

var cardsCmd = &cobra.Command{
	Use:   "cards",
	Short: constants.SEARCH_TEXT + ansi.Color("cards", "cyan"),
	Long:  "\n" + constants.SEARCH_TEXT + ansi.Color("cards", "cyan"),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("\ncard stuff")
	},
}

var setsCmd = &cobra.Command{
	Use:   "sets",
	Short: constants.SEARCH_TEXT + ansi.Color("sets", "green"),
	Long:  "\n" + constants.SEARCH_TEXT + ansi.Color("sets", "green"),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("\nset stuff")
	},
}

var formatsCmd = &cobra.Command{
	Use:   "formats",
	Short: constants.SEARCH_TEXT + ansi.Color("formats", "yellow"),
	Long:  "\n" + constants.SEARCH_TEXT + ansi.Color("formats", "yellow"),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("\nformat stuff")
	},
}
