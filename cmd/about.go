package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	constants "gitlab.com/taylorosbourne/frantic_search/utils"
)

func init() {
	rootCmd.AddCommand(aboutCmd)
}

var aboutMsg = `
📚 For help using this tool, refer to the documentation 📚
  - https://gitlab.com/taylorosbourne/franticsearch/docs

🎉 This project is open-source!  Contributions welcome  🎉 
  - https://gitlab.com/taylorosbourne/franticsearch/

🙌 Data comes from Scryfall!  Consider supporting them  🙌
  - https://scryfall.com/donate
`

var aboutCmd = &cobra.Command{
	Use:   "about",
	Short: "About Frantic Search",
	Long:  constants.ABOUT_FRS,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(cmd.Long)
		fmt.Println(aboutMsg)
	},
}
