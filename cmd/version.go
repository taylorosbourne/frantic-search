package cmd

import (
	"fmt"

	"github.com/mgutz/ansi"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of Frantic Search",
	Long:  "\nAll software has versions. This is Frantic Search's: " + ansi.Color("v0.0.1", "cyan"),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("\nFrantic Search CLI", ansi.Color("v0.0.1", "cyan"), "-- HEAD")
	},
}
